/* List - Header Graphic Animation */
/* ---------------------------------------------------------------------------------------------------- */
var $bottomBarGraphic1 = jQuery('#rbs-bottom-bar-graphic-1');
var $bottomBarGraphic2 = jQuery('#rbs-bottom-bar-graphic-2');
var $topBarGraphic1 = jQuery('#rbs-top-bar-graphic-1');
var $topBarGraphic2 = jQuery('#rbs-top-bar-graphic-2');
var $topBarGraphic2Mobile = jQuery('#rbs-top-bar-graphic-2-mobile');

function listGraphicAnimate() {
	
	if ($bottomBarGraphic1.length > 0) {
		
		$bottomBarGraphic1.addClass('rbs-bottom-bar-animate-top');
		$bottomBarGraphic2.addClass('rbs-bottom-bar-animate-bottom'); 
		$topBarGraphic1.addClass('rbs-top-bar-animate-bottom');
		$topBarGraphic2.addClass('rbs-top-bar-animate-bottom'); 
		$topBarGraphic2Mobile.addClass('rbs-top-bar-animate-bottom');
	}
	
}

/* List - Header Graphic Animation On Resize */
/* ---------------------------------------------------------------------------------------------------- */

function listGraphicAnimateResize() {
	
	if ($bottomBarGraphic2.length > 0) {
		
		var distanceFromTop = $bottomBarGraphic2.offset().top;
		var hideZone = distanceFromTop < 250;
	
		if (window.innerWidth < 1025 && !hideZone) {
			$bottomBarGraphic2.addClass('rbs-reverse-animation');
		} else if (window.innerWidth > 1023) {
			$bottomBarGraphic2.removeClass('rbs-reverse-animation');
		}
	}
}

/* List - Underline Last Word */
/* ---------------------------------------------------------------------------------------------------- */

function underlineLast() {
	var underLineLast = document.getElementsByClassName('rbs-underline-last-wrap');
	
	if (typeof(underLineLast) !== 'undefined' && underLineLast !== null){
		
		for(var i=0;i<underLineLast.length;i++){
			var currentEl = underLineLast[i];
			var button = currentEl.childNodes[1];
			var words = button.innerText.split(/[\s]+/); 
			var lastLine = []; 
			var currentHeight = currentEl.clientHeight; 
			while(1){
			    lastLine.push(words.pop());
			    button.innerText = words.join(' ');
			    if (currentEl.clientHeight < currentHeight) {
				    var span = document.createElement('span');
				    span.classList=['rbs-underline-last'];
				    span.innerText = ' '+lastLine.reverse().join(' ');
				    button.appendChild(span);
				    break;
			    }
			    currentHeight = underLineLast[i].clientHeight;
			    if(!words.length){
			  		break;
			    }
			}
		}
	}
}

/* List - Programme Item Cards Animation */
/* ---------------------------------------------------------------------------------------------------- */
var $window = jQuery(window);

function whenVisibleCards() {
	
	var $itemCards = jQuery('#rbs-programme-list-cards > .rbs-programme-item:not(.rbs-paginated)');
	if ($itemCards.length) {
	
		var visible;
		
		$itemCards.each(function(){
			var $this = jQuery(this);
			var offset = 50;
			
			visible = $window.scrollTop() + window.innerHeight - offset > $this.offset().top + offset/2 && $window.scrollTop() < $this.offset().top + $this.height();
			
	        if (visible) {
				$this.addClass('active');
	        } 
			
			var thisHeight; 
			
			if (window.innerWidth > 545) {
				thisHeight = 180;
			} else if (window.innerWidth < 546 && window.innerWidth > 330){
				thisHeight = 166;
			} else {
				thisHeight = 210;
			}
			
			$window.on('resize', function () {
				
				if (window.innerWidth > 545) {
					thisHeight = 180;
				} else if (window.innerWidth < 546 && window.innerWidth > 330){
					thisHeight = 166;
				} else {
					thisHeight = 210;
				}
				return thisHeight;
			});
			
		    $window.on('scroll', function () {
				
				var distanceTop = $this.offset().top;
				
	        	visible = $window.scrollTop() + window.innerHeight - offset > distanceTop + offset/2 && $window.scrollTop() < distanceTop + thisHeight;

		        if (visible ) {
			        
					$this.addClass('active');
					
		        } else {
			        
			        /* element isn't visible while at the bottom of the window */
			        if ( distanceTop >  $window.scrollTop() ) {
						$this.removeClass('active');
					
					}
		        }
		
		    });
			
		});
	    
	}
	
}

/* List - Programme Item Cards Equal Heights */
/* ---------------------------------------------------------------------------------------------------- */

function programmeStudyCardHeights() {
	
	var $cards = jQuery('#rbs-programme-list-cards > .rbs-programme-item:not(.rbs-paginated)');
	
	$cards.each(function(){
		var $this = jQuery(this);
		
		var cardTotalHeight = $this.height();
		var cardTotalWidth = $this.width();
		var $cardsTop = $this.find('.top').height();
		var $cardsBottom = $this.find('.bottom > p');
		var cardPaddingY = 33;
		var cardPaddingX = 40;
		
		var x = cardTotalHeight - $cardsTop - cardPaddingY;
		var y = cardTotalWidth - cardPaddingX;
		
		var z = Math.floor(x);
		var w = Math.floor(y);
		
		$cardsBottom.css({'height': z + 'px', 'width': w + 'px'});
		
	});
}

/* List - Programme Item Cards Pagination */
/* ---------------------------------------------------------------------------------------------------- */

var $allCards = jQuery('#rbs-programme-list-cards > .rbs-programme-item');

function programmeItemPagination() {
	
	var $button = jQuery('#rbs-programme-item-pagination-wrapper');
	var $container = jQuery('#rbs-programme-list-cards');
	var $hiddenCards = jQuery('#rbs-programme-list-cards > .rbs-paginated');
	var $numberOfAllCards = $allCards.length;
	var numberOfCards = $hiddenCards.length;
	var $buttonText = jQuery('#number');

	if ($hiddenCards.length > 0) {
		console.log($hiddenCards.length);
		if (window.innerWidth > 1023) {
			if ($numberOfAllCards > 9 && !$button.length) {
				$container.append('<ul id="rbs-programme-item-pagination-wrapper"><li id="rbs-programme-item-pagination"><span class="rbs-gradient"></span><span>+ <strong id="number">'+numberOfCards +'</strong> more</span></li></ul>');	
				$buttonText.replaceWith('<strong>' + numberOfCards + '</strong>');
					
			} 
			
		} else {
			if ($numberOfAllCards > 8 && !$button.length) {
				$container.append('<ul id="rbs-programme-item-pagination-wrapper"><li id="rbs-programme-item-pagination"><span class="rbs-gradient"></span><span>+ <strong id="number">'+numberOfCards +'</strong> more</span></li></ul>');	
				$buttonText.replaceWith('<strong>' + numberOfCards + '</strong>');
					
			}
			
		}
		
	} else {
		$button.remove();
	}
	
	//$window.trigger('resize');
	
	$container.one('click', '#rbs-programme-item-pagination', function(){
		
		var $html = jQuery("html, body");
		var scrollToThis = jQuery('.rbs-programme-item:nth-of-type(9)');
		
		$allCards.removeClass('rbs-paginated');
		whenVisibleCards();
		$button.remove();
		programmeStudyCardHeights();
		if (!$('html').hasClass('ie9')) {$(".ellipsis").ellipsis();}	
		
		$html.animate({    // catch the `html, body`
			 scrollTop:scrollToThis.offset().top              // make their `scrollTop` property 0, to go at the top
		}, 400);   
		
	});

}

function programmeItemPaginationReady() {
	
	var $hiddenCards = jQuery('#rbs-programme-list-cards > .rbs-paginated');
	var $numberOfAllCards = $allCards.length;
	var numberOfCards = $hiddenCards.length;
	var $buttonText = jQuery('#number');
	if (window.innerWidth < 1025) {
		
		$allCards.eq(9).removeClass('rbs-paginated');
		$buttonText.replaceWith('<strong>' + numberOfCards + '</strong>');
	
	} else {
		
		$allCards.eq(9).addClass('rbs-paginated');
		$buttonText.replaceWith('<strong>' + numberOfCards + '</strong>');

	}
	
}

/* List - Programme Item Cards Filter */
/* ---------------------------------------------------------------------------------------------------- */

function programmeCardsFilter() {
	
	var $filter = jQuery('#rbs-programme-list-options li');
	var $cards = jQuery('#rbs-programme-list-cards .rbs-programme-item');
	var $button = jQuery('#rbs-programme-item-pagination-wrapper');
	
	$filter.on('click', function(){
		var $container = jQuery('#rbs-filterable-cards');
		var $html = jQuery('html');
		var $this = jQuery(this);
		var filterValue = $this.attr('data-category');
		
		if (!$this.hasClass('active')) {
			$filter.removeClass('active');
			$this.addClass('active');
			programmeStudyCardHeights();
			if (!$('html').hasClass('ie9')) {$(".ellipsis").ellipsis();}	
		}
		
		if (filterValue === 'no-filter') {
			$cards.addClass('filtered');
			
			$window.trigger('scroll');
			programmeStudyCardHeights();
			if (!$('html').hasClass('ie9')) {$(".ellipsis").ellipsis();}	
			
		} else {
			
			$allCards.removeClass('rbs-paginated');
			
			$cards.each(function(){
			
				var $this = jQuery(this);
				var cardValue = $this.attr('data-category');
							
				if (cardValue === filterValue){
					$this.addClass('filtered');
					$this.addClass('active');
				} else {
					$this.removeClass('filtered');
				}
				
			});
			
			$window.trigger('scroll');
			programmeStudyCardHeights();
			if (!$('html').hasClass('ie9')) {$(".ellipsis").ellipsis();}			
			$button.remove();
			
		}
		
		if ($html.hasClass('ie10') || $html.hasClass('ie11') || $html.hasClass('ie')) {
			var evt = document.createEvent('UIEvents');
		     evt.initUIEvent('resize', true, false, window, 0);
		     window.dispatchEvent(evt);
		}
	});
	
}


/* List - Programme Item Ellipses Animation */
/* ---------------------------------------------------------------------------------------------------- */

function whenVisibleEllipse() {
	
	var $ellipse = jQuery('.rbs-ellipses .rbs-ellipse');
	
	if ($ellipse.length) {
	
		var visible;
		
		$ellipse.each(function(){
			var $this = jQuery(this);
			var offset = 100;
			
			visible = $window.scrollTop() + window.innerHeight - offset > $this.offset().top + offset && $window.scrollTop() < $this.offset().top + 200;
			
	        if (visible) {
				$this.addClass('active');
	        } 
			
			var thisHeight; 

			thisHeight = 200;

			$window.on('resize', function () {

				thisHeight = 200;
				
				return thisHeight;
				
			});
			
		    $window.on('scroll', function () {
				
				var distanceTop = $this.offset().top;
				
	        	visible = $window.scrollTop() + window.innerHeight - offset > distanceTop + offset/2 && $window.scrollTop() < distanceTop + thisHeight;

		        if (visible ) {
			        
					$this.addClass('active');
					
		        } else {
			        
			        /* element isn't visible while at the bottom of the window */
			        if ( distanceTop >  $window.scrollTop() ) {
						$this.removeClass('active');
					
					}
		        }
		
		    });
			
		});
	    
	}
	
}

/* Generic - Ellipses on Text */
/* ---------------------------------------------------------------------------------------------------- */

(function($) {
    $.fn.ellipsis = function()
    {
        return this.each(function()
        {
            var el = $(this);

            if(el.css("overflow") == "hidden")
            {
                var text = el.html();
                var multiline = el.hasClass('multiline');
                var t = $(this.cloneNode(true))
                    .hide()
                    .css('position', 'absolute')
                    .css('overflow', 'visible')
                    .width(multiline ? el.width() : 'auto')
                    .height(multiline ? 'auto' : el.height())
                    ;

                el.after(t);

                function height() { return t.height() > el.height(); };
                function width() { return t.width() > el.width(); };

                var func = multiline ? height : width;

                while (text.length > 0 && func())
                {
                    text = text.substr(0, text.length - 1);
                    t.html(text + "...");
                }

                el.html(t.html());
                t.remove();
            }
        });
    };
})(jQuery);


/* List - Learners / Managers Tabs */
/* ---------------------------------------------------------------------------------------------------- */

function learnersManagersTabs() {
	
	var $main = $('.rbs-list');
	
	if ($main.length > 0){
		var $htmlBody = $('html, body');
		var $container = $('.rbs-programme-list-tabs');
		var $learners = $('#learners-view');
		var $managers = $('#managers-view');
		var $learnersButton = $('#rbs-programme-tabs li:nth-child(1), #rbs-tabs-links li:nth-child(1), #footer-tabs-links li:nth-child(1)');
		var $managersButton = $('#rbs-programme-tabs li:nth-child(2), #rbs-tabs-links li:nth-child(2), #footer-tabs-links li:nth-child(2)');
		
		$managersButton.on('click', function(){
			$managers.addClass('active');
			$learners.removeClass('active');
			$managersButton.addClass('active');
			$learnersButton.removeClass('active');
			window.location.hash = '#managers';
			
			$htmlBody.animate({
	            scrollTop: $($container).offset().top
	        }, 350);
			
		});
		
		$learnersButton.on('click', function(){
			$learners.addClass('active');
			$managers.removeClass('active');
			$learnersButton.addClass('active');
			$managersButton.removeClass('active');
			window.location.hash = '#learners';
			if (!$('html').hasClass('ie9')) {$(".ellipsis").ellipsis();}	
			
			$htmlBody.animate({
	            scrollTop: $($container).offset().top
	        }, 350);
			
		});
	
		if (window.location.hash == '#managers') {
			$managers.addClass('active');
			$learners.removeClass('active');
			$managersButton.addClass('active');
			$learnersButton.removeClass('active');
			$('.rbs-ellipse').addClass('active');
			
		} else if (window.location.hash == '#learners') {
			$learners.addClass('active');
			$managers.removeClass('active');
			$learnersButton.addClass('active');
			$managersButton.removeClass('active');
			if (!$('html').hasClass('ie9')) {$(".ellipsis").ellipsis();}	
			
			
		} else {
			underlineLast();
	     	window.location.hash = '#learners';;
	
			$learners.addClass('active');
			$managers.removeClass('active');
			$learnersButton.addClass('active');
			$managersButton.removeClass('active');
	  /*		window.location.hash = '#managers';
	
	  		$managers.addClass('active');
			$learners.removeClass('active');
			$managersButton.addClass('active');
			$learnersButton.removeClass('active');*/
			if (!$('html').hasClass('ie9')) {$(".ellipsis").ellipsis();}	
				
				
				
		}
	}
}


/* List - On Scroll */
/* ---------------------------------------------------------------------------------------------------- */

var listOnScroll =  debounce(function() {
    
    /* List - Programme Item Cards Animation */
	whenVisibleCards();
	
	/* List - Programme Item Ellipse Animation */
	whenVisibleEllipse();

}, 25);

window.addEventListener('scroll', listOnScroll);

/* List - On Resize */
/* ---------------------------------------------------------------------------------------------------- */

optimizedResize.add(function() {
	
	/* List - Header Graphic Animation On Resize */
	listGraphicAnimateResize();
	
	/* List - Underline Last Word */
	underlineLast();
	
	/* List - Programme Item Cards Equal Heights */
	programmeStudyCardHeights();
	
	/* Generic - Ellipses on Text */
	if (!$('html').hasClass('ie9')) {$(".ellipsis").ellipsis();}	
	
});

/* List - Init */
/* ---------------------------------------------------------------------------------------------------- */

window.onload = function(){

	/* List - Programme Item Cards Pagination */
	programmeItemPagination();	
	
	/* List - Programme Item Cards Filter */
	programmeCardsFilter();
	
	
};

window.addEventListener('load', onload);

function listInit() {
    
    /* List - Header Graphic Animation On Resize */
	listGraphicAnimate();
	
	/* List - Programme Item Cards Animation */
	whenVisibleCards();
	
	/* List - Programme Item Ellipse Animation */
	whenVisibleEllipse();
	
	/* List - Programme Item Cards Pagination Ready*/
	programmeItemPaginationReady();
	
	/* List - Programme Item Cards Filter */
	programmeCardsFilter();
	
	/* List - Learners / Managers Tabs */
	learnersManagersTabs();
			
	/* List - Programme Item Cards Equal Heights */
	programmeStudyCardHeights();
	
	/* Generic - Ellipses on Text */
	if (!$('html').hasClass('ie9')) {$(".ellipsis").ellipsis();}	
	
	setTimeout(function(){
		if (!$('html').hasClass('ie9')) {$(".ellipsis").ellipsis();}	
	}, 50);

	underlineLast();
}
