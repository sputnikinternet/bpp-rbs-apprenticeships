/* Programme - Sidebar - Sticky Leaf */
/* ---------------------------------------------------------------------------------------------------- */

function stickyLeafSidebar() {
	
	/* If Multiple Classes Are Present The Smallest One Will Become The Fixed Element */
	var $stickyColumns = jQuery("#rbs-sticky-sidebar-desktop");
	
	var sidebarOffset1 = 10; /* margin-top:-10px on element */
	var sidebarOffset2 = 200; /* sidebar distance from top of page - when at top of page */
	
	var x = sidebarOffset1 + sidebarOffset2;
	
    if (window.innerWidth > 1130) {
		$stickyColumns.stick_in_parent({
    		offset_top: x
	  	});
	  	$stickyColumns.trigger("sticky_kit:recalc");
	} else {
		$stickyColumns.trigger("sticky_kit:detach");
	}

}

/* Programme - Footer CTA - Sticky Leaf */
/* ---------------------------------------------------------------------------------------------------- */

var $stickyFooter = jQuery("#rbs-footer-cta");

function stickyFooter() {
	if ($stickyFooter.length > 0) {
	
		var footerHeight = jQuery('#rbs-footer').height();
		var $cards = jQuery('#rbs-programme-cards');
		var $test = jQuery('#test');
		var windowHeight = window.innerHeight;
		var $list = jQuery('#rbs-filterable-cards');
		var $window = jQuery(window);
		var documentHeight = jQuery(document).height();
		var stickyFooterHeight = $stickyFooter.height();
		if ($cards.length > 0) {
			var startzone = $cards.offset().top;
		}
		if ($list.length > 0){
			var startzone = $list.offset().top;
		}
		var endzone = documentHeight - footerHeight - windowHeight - stickyFooterHeight;
		var fixedZone = endzone - startzone;
	
		if ($window.scrollTop() > endzone)  {
			$stickyFooter.addClass('not-active');
			$stickyFooter.removeClass('active');
			$stickyFooter.addClass('stick-to-bottom');
			$stickyFooter.removeClass('slide-up');
			$stickyFooter.removeClass('stay-up');
			
			
		} else if ($window.scrollTop() < startzone) {
			$stickyFooter.addClass('not-active');
			$stickyFooter.removeClass('active');
			$stickyFooter.removeClass('stick-to-bottom');
			$stickyFooter.removeClass('slide-up');
			$stickyFooter.removeClass('stay-up');
		
		} else if ($window.scrollTop() < fixedZone && $window.scrollTop() < endzone){
			$stickyFooter.addClass('active');
			$stickyFooter.removeClass('not-active');
			$stickyFooter.removeClass('stick-to-bottom');
			$stickyFooter.addClass('slide-up');
			$stickyFooter.removeClass('stay-up');
		
		} else {
			$stickyFooter.addClass('active');
			$stickyFooter.removeClass('not-active');
			$stickyFooter.removeClass('stick-to-bottom');
			$stickyFooter.removeClass('slide-up');
			$stickyFooter.addClass('stay-up');
			
		}
	}
}

/* Programme Item - Downloads - Resize */
/* ---------------------------------------------------------------------------------------------------- */

function downloadsResize() {
	
	var $programmeDowloads = $('.rbs-programme-item-template #rbs-programme-schedule');
	var $footerContainer = $('#rbs-footer-container');
	var $stickySidebar = $('#rbs-sticky-sidebar-desktop');
	
	if ($programmeDowloads.length > 0) {
		
		if (window.innerWidth >= 1130) {
			
			$programmeDowloads.prependTo($footerContainer);
			
		} else {
			
			$programmeDowloads.appendTo($('#rbs-programme'));
			
		}
		
	}
	
}

/* Programme - Footer CTA - Button */
/* ---------------------------------------------------------------------------------------------------- */

function footerCta() {
	
	var $scrollTo = jQuery('#rbs-scroll-to');
	var $htmlbody = jQuery('html, body');
	var $form = jQuery('#rbs-programme');
	
	if ($scrollTo.length) {
	
	    $scrollTo.on('click', function(e){
		    e.preventDefault();
	        $htmlbody.animate({
	            scrollTop: jQuery($form).offset().top
	        }, 400);
	    });
    
    }
}

/* Programme - Cards Equal Height */
/* ---------------------------------------------------------------------------------------------------- */

function programmeCardsEqualHeight() {
	
	var $cards = jQuery('#rbs-programme-cards article > div');
	
	setTimeout(function(){
		equalizeHeights($cards);
	}, 75);
	
}

/* Programme - Cards Tab Content */
/* ---------------------------------------------------------------------------------------------------- */

var $cardsTabContent = jQuery('#rbs-programme-cards .rbs-tab-content');
var $cardsTabContainer = jQuery('#rbs-tab-content-container');
var $card1 = jQuery('#rbs-time');
var $card2 = jQuery('#rbs-cost');
var $card3 = jQuery('#rbs-study');
var $card4 = jQuery('#rbs-qualifications');
var $tab1 = jQuery('#rbs-time-content');
var $tab2 = jQuery('#rbs-cost-content');
var $tab3 = jQuery('#rbs-study-content');
var $tab4 = jQuery('#rbs-qualifications-content');

function programmeCardsTabContentResize() {
	
	/* Insert Card Tabs Into Tabs Container - Get Heights */
	$cardsTabContent.each(function(){
		var passCondition2 = jQuery(this).parent().is($cardsTabContainer);
		var passCondition3 = $tab2.prev().is('article');
		
		if (window.innerWidth > 767) {
			if (!passCondition2) {
				jQuery(this).prependTo($cardsTabContainer);
				
			}
		} else {
			if (!passCondition3) {
				
				$tab1.insertAfter($card1);
				$tab2.insertAfter($card2);
				$tab3.insertAfter($card3);
				$tab4.insertAfter($card4);
			}
		}
	});
	
}

function programmeCardsTabContentClicks() {
		/* Show Tab When Card Is Clicked */
	$card1.on('click', function(){
		$card2.removeClass('active');
		$card3.removeClass('active');
		$card4.removeClass('active');
		$tab2.removeClass('active');
		$tab3.removeClass('active');
		$tab4.removeClass('active');
		if ($tab1.hasClass('active')){
			$card1.removeClass('active');
			$tab1.removeClass('active');
		} else {
			$card1.addClass('active');
			$tab1.addClass('active');
		}
	});
	$card2.on('click', function(){
		$card1.removeClass('active');
		$card3.removeClass('active');
		$card4.removeClass('active');
		$tab1.removeClass('active');
		$tab3.removeClass('active');
		$tab4.removeClass('active');
		if ($tab2.hasClass('active')){
			$card2.removeClass('active');
			$tab2.removeClass('active');
		} else {
			$card2.addClass('active');
			$tab2.addClass('active');
		}
	});
	$card3.on('click', function(){
		$card1.removeClass('active');
		$card2.removeClass('active');
		$card4.removeClass('active');
		$tab1.removeClass('active');
		$tab2.removeClass('active');
		$tab4.removeClass('active');
		if ($tab3.hasClass('active')){
			$card3.removeClass('active');
			$tab3.removeClass('active');
		} else {
			$card3.addClass('active');
			$tab3.addClass('active');
		}
	});
	$card4.on('click', function(){
		$card1.removeClass('active');
		$card2.removeClass('active');
		$card3.removeClass('active');
		$tab1.removeClass('active');
		$tab2.removeClass('active');
		$tab3.removeClass('active');
		if ($tab4.hasClass('active')){
			$card4.removeClass('active');
			$tab4.removeClass('active');
		} else {
			$card4.addClass('active');
			$tab4.addClass('active');
		}
	});
}

/* Programme - Tooltip */
/* ---------------------------------------------------------------------------------------------------- */

function programmeTooltips() {
	
	var $helper = jQuery('.rbs-helper-container');
	
	$helper.each(function(){
		
		var $this = jQuery(this);
		var $tooltip = $this.find('.rbs-tooltip');
		
		$this.on('click', ':not(.rbs-tooltip)', function(){

			if ($tooltip) {
				$this.toggleClass('active');
			}
			
		});

	});
	
}

/* Programme - Hide Tooltip When Out Of View */
/* ---------------------------------------------------------------------------------------------------- */

function whenVisibleTooltip() {
	
	var $tooltip = jQuery('.rbs-helper-container.active');
	
	if ($tooltip.length) {
	
		var $window = jQuery(window);
		var visible;
		
		var hasRun = true;
	
	    $window.on('scroll', function () {
	
        	visible = $window.scrollTop() + window.innerHeight > $tooltip.offset().top && $window.scrollTop() < $tooltip.offset().top + $tooltip.height();
				
	        if (!visible) {
		        
				if (hasRun) {
					$tooltip.removeClass('active');
				} 
				
				hasRun = false;
				
	        }
	
	    });
		
		return hasRun;
	
	}
	
}

/* Programme - Study Accordion */
/* ---------------------------------------------------------------------------------------------------- */
function programmeStudyBuild() {

	var $accordionContent = jQuery('#rbs-programme-study-accordion .rbs-accordion-content');
	var $liClass = jQuery('#rbs-programme-study-accordion li');
	
	var condition1 = $accordionContent.parent().is('#rbs-programme-study-accordion > li > ul');
	var condition2 = $accordionContent.parent().is('#rbs-programme-study-accordion > li > ul > li');
	
	$accordionContent.each(function(){
		
		var $this = jQuery(this);
		var $contentContainer = $this.parent().parent();
		
	
		if (window.innerWidth > 767) {
			if (!condition1 || condition2) {
				
				$liClass.removeClass('active');
				$accordionContent.removeClass('active');
				$this.appendTo($contentContainer);
			}
		} else {

			if (condition1) {
		
				var $liFirst = $this.parent().children('li.rbs-first');
				var $contentFirst = $this.filter('.rbs-first');
				var $liLast = $this.parent().children('li.rbs-last');
				var $contentLast = $this.filter('.rbs-last');

				$liClass.removeClass('active');
				$accordionContent.removeClass('active');
				$contentFirst.appendTo($liFirst);
				$contentLast.appendTo($liLast);
			}			
		}
	});
}

function programmeStudyAccordion() {
	
	var $accordionbutton = jQuery('#rbs-programme-study-accordion .rbs-accordion-button');
	
	$accordionbutton.on('click', function(e){
		
		var $liFirst = jQuery(e.target).parent().parent().is('.rbs-first');
		var $liLast = jQuery(e.target).parent().parent().is('.rbs-last');
		
		var $liLastActive = jQuery(e.target).parent().parent().next().is('.rbs-last.active');
		var $liFirstActive = jQuery(e.target).parent().parent().prev().is('.rbs-first.active');
		
		var $contentFirst = jQuery(e.target).parent().parent().parent().find('.rbs-accordion-content.rbs-first');
		var $contentLast = jQuery(e.target).parent().parent().parent().find('.rbs-accordion-content.rbs-last');
		
		var $contentContainer = jQuery(e.target).parent().parent().parent().parent().find('ul');
		
		var $this = jQuery(this);
		var $li = $this.parent();
		
		if (window.innerWidth > 767) {
			if ($liFirst && $liLastActive) {
					
				$li.addClass('active');
				$contentFirst.addClass('active');
				$li.next().removeClass('active');
				$contentLast.removeClass('active');
				$contentLast.appendTo($contentContainer);
				
				
			} else if ($liLast && $liFirstActive) {
					
				$li.addClass('active');
				$contentLast.addClass('active');
				$li.prev().removeClass('active');
				$contentFirst.removeClass('active');
				$contentFirst.appendTo($contentContainer);
				
				
			} else if ($liFirst && !$liLastActive) {
				
				if (!$li.hasClass('active')) {
					$li.addClass('active');
					$contentFirst.addClass('active');
				} else {
					$li.removeClass('active');
					$contentFirst.removeClass('active');
				}
		
				
			} else if ($liLast && !$liFirstActive) {
				
				if (!$li.hasClass('active')) {
					$li.addClass('active');
					$contentLast.addClass('active');
				} else {
					$li.removeClass('active');
					$contentLast.removeClass('active');
				}
				
			} 
		} else {
			if ($liFirst) {
				$li.toggleClass('active');
				$contentFirst.toggleClass('active');
			} else if ($liLast) {
				$li.toggleClass('active');
				$contentLast.toggleClass('active');
			}
		}
		
	});

}

function programmeStudyButtonHeights() {
	
	var $accordionButton = jQuery('#rbs-programme-study-accordion .rbs-accordion-button');
	
	setTimeout(function(){
		equalizeHeights($accordionButton);
	}, 75);
	
}

/* Programme - Mobile Tabs Menu */
/* ---------------------------------------------------------------------------------------------------- */

function programmeMobileTabsMenu() {
	
	var $hamburger = $('#rbs-hamburger');
	var $mobileMenu = $('#rbs-mobile-tabs-tray');
	
	$hamburger.on('click', function(){
		
		var $this = $(this);
		
		if ($this.hasClass('active')) {
			
			$this.removeClass('active');
			$mobileMenu.removeClass('active');
			
		} else {
			
			$this.addClass('active');
			$mobileMenu.addClass('active');
			
		}
		
	});
	
}

/* Programme - On Resize */
/* ---------------------------------------------------------------------------------------------------- */

optimizedResize.add(function() {
	
	/* Programme - Sidebar Responsive */
  //  programmeSidebarResponsive();
    
    /* Programme - Cards Equal Height */
    programmeCardsEqualHeight();
 
    /* Programme - Cards Tab Content */
 	programmeCardsTabContentResize();
    
    /* Programme - Study Accordion */
    programmeStudyBuild();
    programmeStudyButtonHeights();
	
	/* Programme - Sticky Leaf - Recalcalate */
    stickyLeafSidebar();
    
    /* Programme Item - Downloads - Resize */
	downloadsResize();
    
});


/* Programme - On Scroll */
/* ---------------------------------------------------------------------------------------------------- */

var programmeOnScroll = function() {
    
    /* Programme - Hide Tooltip When Out Of View */
    whenVisibleTooltip();
    stickyFooter();
};

window.addEventListener('scroll', programmeOnScroll);

/* Programme - Init */
/* ---------------------------------------------------------------------------------------------------- */

function programmeInit() {
    
    /* Programme - On Resize */
    // programmeSidebarResponsive();
    
    /* Programme - Sidebar - Sticky Leaf */
    stickyLeafSidebar();
    
    /* Programme - Cards Tab Content */
    programmeCardsTabContentResize();
    // programmeCardsTabContentClicks();
    
    /* Programme - Cards Equal Height */
    programmeCardsEqualHeight();
    
    /* Programme - Tooltip */
	programmeTooltips();
	
    /* Programme - Study Accordion */
    programmeStudyBuild();
    programmeStudyAccordion();
    programmeStudyButtonHeights();
    
    /* Programme - Sticky Leaf Fixed Els */
	stickyFooter();

	/* Programme - Footer CTA Button */
	footerCta();
	
	/* Programme Item - Downloads - Resize */
	downloadsResize();
	
	/* Programme - Mobile Tabs Menu */
	programmeMobileTabsMenu();

}
