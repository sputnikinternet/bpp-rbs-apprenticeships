/* Detect Platform */
/* ---------------------------------------------------------------------------------------------------- */
function detectPlatform() {
	
	var getMobileOperatingSystem = function() {
		
	    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
	
	    // Windows Phone must come first because its UA also contains “Android”
	    if (/windows phone/i.test(userAgent)) {
	        return "Windows Phone";
	
	    }
	
	    if (/android/i.test(userAgent)) {
	        return "Android";
	    }
	
	    // iOS detection from: http://stackoverflow.com/a/9039885/177710
	    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
	        return "iOS";
	    }
	
	    return "unknown";
	    
	};
	
	var $html = jQuery('html');
	
	if (getMobileOperatingSystem() === "unknown") {
		
	    $html.addClass('platform-desktop');
	    
	} else {
		
	    $html.addClass('platform-mobile');
	    
	}
}

/* Detect Browser */
/* ---------------------------------------------------------------------------------------------------- */

function detectBrowser() {
	
	var $html = jQuery('html');
	
	/* Opera 8.0+ (UA detection to detect Blink/v8-powered Opera) */
    var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    if (isOpera) {
        $html.addClass('opera');
    }
    /* Firefox 1.0+ */
    var isFirefox = typeof InstallTrigger !== 'undefined';
    if (isFirefox) {
        $html.addClass('firefox');
    }
    /* Safari 3.0+ */
    var isSafari = /constructor/i.test(window.HTMLElement);
    if (isSafari || (function (p) {
        return p.toString() === "[object SafariRemoteNotification]";
    })(!window.safari || window.safari.pushNotification)) {
        $html.addClass('safari');
    }
    /* Internet Explorer 6-11 */
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    if (isIE) {
        $html.addClass('ie');
    }

	if (navigator.userAgent.match('MSIE 10.0;')) {
	  $html.addClass('ie10');
	}
    /* Edge 20+ */
    var isEdge = !isIE && !!window.StyleMedia;
    if (isEdge) {
        $html.addClass('edge');
    }
    /* Chrome 1+ */
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    if (isChrome) {
        $html.addClass('chrome');
    }
    /* Blink engine detection */
    var isBlink = (isChrome || isOpera) && !!window.CSS;
    if (isBlink) {
        $html.addClass('blink');
    }

    /* Chrome */
    if (/Chrome/.test(navigator.userAgent) && /Safari/.test(navigator.userAgent)) {
        $html.addClass('chrome');
    }

    /* Safari */
    if (/Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgent)) {
        $html.addClass('safari');
    }
	
}

/* Detect OS */
/* ---------------------------------------------------------------------------------------------------- */

function detectOs() {
	
	function getOS() {
	  
	    var userAgent = window.navigator.userAgent,
	        platform = window.navigator.platform,
	        macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
	        windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
	        iosPlatforms = ['iPhone', 'iPad', 'iPod'],
	        os = null;
	
	    if (macosPlatforms.indexOf(platform) !== -1) {
	        os = 'mac';
	    } else if (iosPlatforms.indexOf(platform) !== -1) {
	    	os = 'ios';
	    } else if (windowsPlatforms.indexOf(platform) !== -1) {
	    	os = 'windows';
	    } else if (/Android/.test(userAgent)) {
	    	os = 'android';
	    } else if (!os && /Linux/.test(platform)) {
	    	os = 'linux';
	    }
	
	  return os;
	  
	}
	
	var $html = jQuery('html');
	
	$html.addClass(getOS());
	
}

/* Detect - Init */
/* ---------------------------------------------------------------------------------------------------- */

function detectInit() {
	
	/* Detect Platform */
	detectPlatform();
	
	/* Detect Browser */
	detectBrowser();
	
	/* Detect OS */
	detectOs();
	
}