/* Generic - Debounce */
/* ---------------------------------------------------------------------------------------------------- */

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) {
				func.apply(context, args);
			}
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) {
			func.apply(context, args);
		}
	};
}

/* Generic - Optimised Resize */
/* ---------------------------------------------------------------------------------------------------- */
var optimizedResize = (function() {

    var callbacks = [],
        running = false;

    // fired on resize event
    function resize() {

        if (!running) {
            running = true;

            if (window.requestAnimationFrame) {
                window.requestAnimationFrame(runCallbacks);
            } else {
                setTimeout(runCallbacks, 66);
            }
        }

    }

    // run the actual callbacks
    function runCallbacks() {

        callbacks.forEach(function(callback) {
            callback();
        });

        running = false;
    }

    // adds callback to loop
    function addCallback(callback) {

        if (callback) {
            callbacks.push(callback);
        }

    }

    return {
        // public method to add additional callback
        add: function(callback) {
            if (!callbacks.length) {
                window.addEventListener('resize', resize);
            }
            addCallback(callback);
        }
    };
}());


/* Generic - Equalize Heights - All */
/* ---------------------------------------------------------------------------------------------------- */

function equalizeHeights(selector) {
	var heights = [];
	
	jQuery(selector).each(function() {

		jQuery(this).css('min-height', '0');
		jQuery(this).css('max-height', 'none');
		jQuery(this).css('height', 'auto');

 		heights.push(jQuery(this).height());
	});

	var max = Math.max.apply( Math, heights );

	jQuery(selector).each(function() {
		jQuery(this).css('height', max + 'px');
	});	
}


/* Generic - Init */
/* ---------------------------------------------------------------------------------------------------- */

function genericInit() {

    
}
