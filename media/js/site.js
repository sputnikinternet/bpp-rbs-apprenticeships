/* Codekit Import - Libs */
/* ---------------------------------------------------------------------------------------------------- */
// @codekit-prepend "libs/sticky-kit.js";

/* Codekit Import - Site */
/* ---------------------------------------------------------------------------------------------------- */
// @codekit-prepend "includes/detect.js";
// @codekit-prepend "includes/generic.js";
// @codekit-prepend "includes/programme.js";
// @codekit-prepend "includes/list.js";

/* Site Ready */
/* ---------------------------------------------------------------------------------------------------- */
jQuery(document).ready(function() {

    /* Detect - Init */
    detectInit();
    
    /* Generic - Init */
    genericInit();
    
    /* Programme - Init */
    programmeInit();
    
	/* List - Init */
    listInit();
    
});